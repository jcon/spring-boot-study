//package com.jcon.redis.config;
//
//import io.lettuce.core.RedisClient;
//import io.lettuce.core.RedisURI;
//import io.lettuce.core.api.StatefulRedisConnection;
//import io.lettuce.core.resource.ClientResources;
//import io.lettuce.core.resource.DefaultClientResources;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class LettuceConfig {
//
//    @Bean(destroyMethod = "shutdown")
//    ClientResources clientResources() {
//        return DefaultClientResources.create();
//    }
//
//    @Bean(destroyMethod = "shutdown")
//    RedisClient redisClient(ClientResources clientResources) {
//        return RedisClient.create(clientResources, RedisURI.create(TestSettings.host(), TestSettings.port()));
//    }
//
//    @Bean(destroyMethod = "close")
//    StatefulRedisConnection<String, String> connection(RedisClient redisClient) {
//        return redisClient.connect();
//    }
//}