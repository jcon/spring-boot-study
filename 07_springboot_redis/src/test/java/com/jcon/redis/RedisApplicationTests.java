package com.jcon.redis;

import io.lettuce.core.api.sync.RedisStreamCommands;
import io.lettuce.core.resource.DefaultClientResources;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisApplicationTests {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    DefaultClientResources clientResources;
    @Autowired
    LettuceConnectionFactory lettuceConnectionFactory;

	@Test
	public void contextLoads() {
	    stringRedisTemplate.opsForValue().set("hello", "test");
	}

	@Test
    public void factory() {
        System.out.println(lettuceConnectionFactory);
    }

}
