package com.jcon.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (控制层类)
 * @date 2018年8月12日
 */
@RestController
public class MyController {

    @RequestMapping("/hello")
    public Object hello() {
        System.out.println("hello world!");
        return "hello, world!test";
    }

}
