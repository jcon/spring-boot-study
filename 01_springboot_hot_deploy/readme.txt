springboot热部署的两种方式：
1.方式一
	在pom.xml中加入springboot热部署的jar坐标，启动时用eclipse工具中的run as或者java -jar启动
	优点是启动的时候比较方便
	缺点是每次修改代码之后都回进行重启
2.方式二：
	在pom.xml的build中加入maven插件，启动时要使用maven的方式来启动clean spring-boot:run
	优点是每次修改代码之后不用重启就自动生效
	缺点是启动的时候要以maven的方式启动
	