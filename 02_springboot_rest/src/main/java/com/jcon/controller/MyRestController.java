package com.jcon.controller;

import com.jcon.vo.CommonVo;
import com.jcon.vo.Response;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (REST风格控制器)
 * @date 2018年09月16日
 */
@RestController
@RequestMapping("/rest")
public class MyRestController {

    @PostMapping("/users")
    @Description("增加用户")
    public Object addUsres(@RequestBody String json) {
        return Response.success(new CommonVo("true"));
    }

    @PutMapping("/usres/{id}")
    @Description("修改用户(修改对象的多个属性值)")
    public Object updateUsers(@RequestBody String josn) {
        return Response.success(josn);
    }

    @PatchMapping("/usres/name/{id}")
    @Description("修改用户(修改对象单个属性值name)")
    public Object updateUsresName(@RequestBody String josn) {
        return Response.success(josn);
    }

    @GetMapping("/users")
    @Description("查询用户")
    public Object query(@RequestParam(value = "data") String json) {
        return Response.success(json);
    }

    @DeleteMapping("/users/{id}")
    @Description("删除用户")
    public Object delete(@RequestParam(value = "data") String json) {
        return Response.error();
    }

}
