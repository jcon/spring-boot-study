package com.jcon.vo;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (响应信息体)
 * @date 2018年09月16日
 */
public class Message<T> {
    private String code;
    private String msg;
    private T data;

    public Message() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
