package com.jcon.vo;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (统一响应数据实体类)
 * @date 2018年09月16日
 */
public class Response {

    public static Message success(Object data) {
        Message message = new Message();
        message.setCode("0");
        message.setMsg("请求成功");
        message.setData(data);
        return  message;
    }

    public static Message error() {
        Message message = new Message();
        message.setCode("9999");
        message.setMsg("服务器异常");
        return message;
    }

}
