package com.jcon.vo;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (公共响应数据)
 * @date 2018年09月16日
 */
public class CommonVo {
    private String result;

    public CommonVo(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
