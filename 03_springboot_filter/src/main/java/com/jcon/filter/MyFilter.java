package com.jcon.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author zjc
 * @version V1.0
 * @Description: (过滤器)
 * @date 2018年09月06日
 */
public class MyFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("******执行过滤器中的doFilter方法*****");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
