package com.jcon;

import com.jcon.filter.MyFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Application {

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean myFilterBean = new FilterRegistrationBean();
		myFilterBean.setFilter(new MyFilter());
		myFilterBean.setOrder(1);
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/*");
		myFilterBean.setUrlPatterns(urlPatterns);
		return myFilterBean;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
