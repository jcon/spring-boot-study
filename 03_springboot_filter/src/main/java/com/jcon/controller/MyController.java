package com.jcon.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zjc
 * @version V1.0
 * @Description: (控制器)
 * @date 2018年09月06日
 */
@RestController
public class MyController {

    @GetMapping("/hello")
    public String index(){
        System.out.println("=========执行控制器========");
        return "success!";
    }

}
