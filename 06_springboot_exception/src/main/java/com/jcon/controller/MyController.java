package com.jcon.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (控制层)
 * @date 2018年12月03日
 */
@RestController
@RequestMapping
public class MyController {

    @GetMapping("/test")
    public Object test() throws Exception{
        System.out.println("-----------");
        return 1/0;
    }
}
