package com.jcon.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (全局异常处理类)
 * @date 2018年12月03日
 */
@ControllerAdvice
@ResponseBody
public class GlobalException {

    private static Logger logger = LoggerFactory.getLogger(GlobalException.class);

    @ExceptionHandler(value = Exception.class)
    public Object serviceExceptionHandler(HttpServletRequest request, Exception exception)
            throws Exception {
        System.out.println("-----出现异常----" + exception.getMessage());
        return "出现异常!";
    }
}
