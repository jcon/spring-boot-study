package com.jcon.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (控制层)
 * @date 2018年12月02日
 */
@RestController
public class MyController {

    @GetMapping("test")
    public Object test(){
        return "测试";
    }

}
