package com.jcon;

import com.jcon.config.Person_Method01;
import com.jcon.config.Person_Method02;
import com.jcon.config.Person_Method03;
import com.jcon.config.TestStatic;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ApplicationTests {

    @Autowired
    Person_Method01 person_method01;
    @Autowired
    Person_Method02 person_method02;

    // 测试获取配置文件中的值
    // 通过@ConfigurationProperties的方式
    @Test
    public void testMethod01() {
        log.info(person_method01.toString());
    }

    // 测试获取配置文件中的值
    // 通过@Value的方式
    @Test
    public void testMethod02() {
        log.info(person_method02.toString());
    }


    // 测试获取配置文件中的值
    // 通过静态类的方式
    @Test
    public void testMethod03() {
        log.info(Person_Method03.getName());
        log.info(Person_Method03.getAge()+"");
        log.info(Person_Method03.getBirth().toString());
        log.info("-----test.age={}-----", TestStatic.age);
    }
}

