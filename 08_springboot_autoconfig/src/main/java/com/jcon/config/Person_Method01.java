package com.jcon.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;


import javax.validation.constraints.Max;
import java.util.Date;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (配置实体类)
 * @date 2018年12月16日
 */
@Component
@ConfigurationProperties(prefix = "person")
@Data
//@Validated
public class Person_Method01 {

    private String name;
    @Max(value = 100)
    private int age;
    private Date birth;

}
