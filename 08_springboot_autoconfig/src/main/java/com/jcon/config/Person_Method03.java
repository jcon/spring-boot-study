package com.jcon.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (方法三获取配置文件值)
 * @date 2018年12月27日
 */
@Component
public class Person_Method03 {

    private static String name;
    private static int age;
    private static Date birth;

    @Value("${person.name}")
    private void setName(String name) {
        Person_Method03.name = name;
    }
    @Value("${person.age}")
    private void setAge(int age) {
        Person_Method03.age = age;
    }
    @Value("${person.birth}")
    private void setBirth(Date birth) {
        Person_Method03.birth = birth;
    }

    public static String getName() {
        return name;
    }

    public static int getAge() {
        return age;
    }

    public static Date getBirth() {
        return birth;
    }
}
