package com.jcon.config;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (测试类)
 * @date 2018年12月27日
 */
public class TestStatic {

    public static int age = 0;

    static {
        age = Person_Method03.getAge();
        System.out.println("---------------");
        System.out.println(age);
    }
}
