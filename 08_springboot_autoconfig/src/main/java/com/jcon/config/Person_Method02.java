package com.jcon.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author Jcon
 * @version V1.0
 * @Description: (方法二获取配置的数值)
 * @date 2018年12月27日
 */
@Component
@Data
public class Person_Method02 {

    @Value("${person.name}")
    private String name;
    @Value("${person.age}")
    private int age;
    @Value("${person.birth}")
    private Date birth;

}
